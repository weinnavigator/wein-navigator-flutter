import 'package:flutter/material.dart';
import 'package:wein_navigator/event.dart';
import 'package:wein_navigator/event_detail_view.dart';
import 'package:wein_navigator/slide_left_route.dart';

class _EventDescription extends StatelessWidget {
  _EventDescription(
      {Key key,
      this.title,
      this.subtitle,
      this.date,
      this.time,
      this.location,
      this.distance})
      : super(key: key);

  final String title;
  final String subtitle;
  final String date;
  final String time;
  final String location;
  final String distance;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 2,
          child: Row(children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  '$subtitle',
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      fontSize: 18.0,
                      color: Colors.grey,
                      fontFamily: 'Kaufmann'),
                ),
                const Padding(padding: EdgeInsets.only(bottom: 2.0)),
                Text(
                  '$title',
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(fontSize: 22, color: Colors.white),
                ),
              ],
            ),
            new Spacer(),
            Icon(
              Icons.arrow_forward_ios,
              color: Colors.white,
              size: 15.0,
            ),
          ]),
        ),
        Expanded(
          flex: 1,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                    child: Row(
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Icon(
                          Icons.access_time,
                          color: Colors.white,
                          size: 15.0,
                        )),
                    const Padding(padding: EdgeInsets.only(right: 5.0)),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        const Padding(padding: EdgeInsets.only(top: 7.0)),
                        Text(
                          '$date',
                          style: const TextStyle(
                            fontSize: 14.0,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          '$time',
                          style: const TextStyle(
                            fontSize: 12.0,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ],
                )),
              ),
              Expanded(
                flex: 1,
                child: Container(
                    child: Row(
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.only(top: 6.0),
                        child: Icon(
                          Icons.location_on,
                          color: Colors.white,
                          size: 15.0,
                        )),
                    const Padding(padding: EdgeInsets.only(right: 5.0)),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        const Padding(padding: EdgeInsets.only(top: 7.0)),
                        Text(
                          '$location',
                          style: const TextStyle(
                            fontSize: 14.0,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          '$distance',
                          style: const TextStyle(
                            fontSize: 12.0,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ],
                )),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class EventListItem extends StatelessWidget {
  EventListItem(
      {Key key,
      this.thumbnail,
      this.title,
      this.subtitle,
      this.date,
      this.time,
      this.location,
      this.distance,
      this.event})
      : super(key: key);

  final Widget thumbnail;
  final String title;
  final String subtitle;
  final String date;
  final String time;
  final String location;
  final String distance;
  final Event event;

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
        onTap: () {
          Navigator.push(
              context, SlideLeftRoute(page: EventDetailView(event: event)));
        },
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage('assets/even_item_bg.jpg'),
            fit: BoxFit.cover,
          )),
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: SizedBox(
            height: 100,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  decoration: new BoxDecoration(
                    border: new Border.all(color: Colors.white, width: 1),
                  ),
                  child: AspectRatio(
                    aspectRatio: 1.2,
                    child: thumbnail,
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(20.0, 0.0, 2.0, 0.0),
                    child: _EventDescription(
                        title: title,
                        subtitle: subtitle,
                        date: date,
                        time: time,
                        location: location,
                        distance: distance),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
