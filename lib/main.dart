import 'dart:async' show Future;
import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:wein_navigator/event.dart';
import 'package:wein_navigator/event_list_item.dart';

void main() => runApp(WeinNavigator());

class WeinNavigator extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Wein Navigator',
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.red,
          fontFamily: 'NeutraText'),
      home: Scaffold(
        appBar: AppBar(
            title: Text(
              'Events',
              style: TextStyle(fontFamily: 'NeutraText', fontSize: 23),
            ),
            backgroundColor: Color.fromRGBO(105, 7, 7, 1.0)),
        body: Container(
          child: ListLayout(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              stops: [0, 1],
              colors: [
                Color.fromRGBO(110, 4, 0, 1),
                Color.fromRGBO(70, 3, 0, 1)
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ListLayout extends StatefulWidget {
  @override
  ListLayoutState createState() => new ListLayoutState();
}

class ListLayoutState extends State<ListLayout> {
  var events = new List<Event>();

  Future<String> getData() async {
    var response = await rootBundle.loadString('assets/events.json');

    this.setState(() {
      List parsedJson = json.decode(response);
      for (int x = 0; x < parsedJson.length; x++) {
        events.add(new Event(parsedJson[x]));
      }
    });

    return "Success!";
  }

  @override
  void initState() {
    super.initState();
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: events == null ? 0 : events.length,
      separatorBuilder: (BuildContext context, int index) => Container(
        color: Colors.white,
        height: 1,
      ),
      itemBuilder: (BuildContext context, int index) {
        Event event = events[index];
        return EventListItem(
            thumbnail: Container(
              child: Image.network(
                event.previewUrl,
                fit: BoxFit.cover,
              ),
            ),
            title: event.eventName,
            subtitle: event.category,
            date: event.eventDate.toDateString(),
            time: event.eventDate.toTimeString(),
            location: event.eventLocation.city,
            distance: event.distance,
            event: event);
      },
    );
  }
}
