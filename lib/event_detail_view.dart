import 'package:flutter/material.dart';
import 'package:wein_navigator/event.dart';

class EventDetailView extends StatelessWidget {
  final Event event;

  EventDetailView({Key key, this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final topContentText = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Text(
          event.category,
          style: TextStyle(
            color: Colors.white,
            fontSize: 32,
            fontFamily: "Kaufmann",
            shadows: <Shadow>[
              Shadow(
                offset: Offset(1.0, 1.0),
                blurRadius: 0.0,
                color: Color.fromRGBO(0, 0, 0, 1),
              ),
            ],
          ),
        ),
        Text(
          event.eventName,
          style: TextStyle(
            color: Colors.white,
            fontSize: 40.0,
            fontFamily: "NeutraText",
            shadows: <Shadow>[
              Shadow(
                offset: Offset(2.0, 2.0),
                blurRadius: 0.0,
                color: Color.fromRGBO(0, 0, 0, 1),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 30,
        )
      ],
    );

    final topContent = Stack(
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(left: 10.0),
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new NetworkImage(event.imageUrl),
                fit: BoxFit.cover,
              ),
              border: Border(
                bottom: BorderSide(width: 2.0, color: Colors.white),
              ),
            )),
        Container(
          height: MediaQuery.of(context).size.height * 0.5,
          padding: EdgeInsets.only(left: 20),
          width: MediaQuery.of(context).size.width,
          child: topContentText,
        ),
        Positioned(
          left: 8.0,
          top: 30.0,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, size: 30, color: Colors.white),
          ),
        )
      ],
    );

    bottomBox(icon, text1, text2) {
      return Container(
          height: 40,
          width: MediaQuery
              .of(context)
              .size
              .width * 0.467,
          margin: EdgeInsets.all(5),
          decoration: new BoxDecoration(
              color: Color.fromRGBO(105, 7, 7, 1.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.black,
                  blurRadius: 2.0,
                ),
              ]),
          child: Row(
            children: <Widget>[
              Container(
                height: double.infinity,
                padding: EdgeInsets.all(10),
                decoration: new BoxDecoration(
                    color: Color.fromRGBO(105, 7, 7, 1.0),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black,
                        blurRadius: 1.0,
                      ),
                    ]),
                child: Icon(Icons.store, color: Colors.white, size: 15.0),
              ),
              Expanded(
                  flex: 1,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          text1,
                          style: TextStyle(color: Colors.white),
                        ),
                        Text(
                          text2,
                          style: TextStyle(color: Colors.grey, fontSize: 13),
                        ),
                      ]))
            ],
          ));
    }

    final bottomContent = Container(
        width: MediaQuery
            .of(context)
            .size
            .width,
        padding: EdgeInsets.only(left: 5, top: 5),
        height: MediaQuery
            .of(context)
            .size
            .height * 0.5,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0, 1],
            colors: [Color.fromRGBO(110, 4, 0, 1), Color.fromRGBO(70, 3, 0, 1)],
          ),
        ),
        child: Row(children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              bottomBox(Icons.alarm, event.eventDate.toDateString(),
                  event.eventDate.toTimeString()),
              bottomBox(Icons.wb_sunny, event.temp, event.weather),
              bottomBox(
                  Icons.location_on, event.eventLocation.city, event.distance)
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              bottomBox(Icons.refresh, event.way, event.wayTime),
              bottomBox(Icons.store, event.stationNum.toString(), "")
            ],
          ),
        ]));

    return Scaffold(
      body: Column(
        children: <Widget>[topContent, bottomContent],
      ),
    );
  }
}
