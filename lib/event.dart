import 'package:intl/intl.dart';

class Event {
  String eventName;
  String category;
  EventDate eventDate;
  EventLocation eventLocation;
  String previewUrl;
  String distance;
  String temp;
  String weather;
  String way;
  String wayTime;
  int stationNum;
  String imageUrl;

  Event(jsObject) {
    this.eventName = jsObject["name"];
    this.category = jsObject["category"];
    this.eventDate = new EventDate(jsObject["eventDate"]["start"], jsObject["eventDate"]["end"]);
    this.eventLocation = new EventLocation(
        jsObject["eventLocation"]["zip"],
        jsObject["eventLocation"]["city"],
        jsObject["eventLocation"]["lat"],
        jsObject["eventLocation"]["lon"]);
    this.previewUrl = jsObject["previewUrl"];
    this.imageUrl = jsObject["imageUrl"];
    this.distance = jsObject["distance"];
    this.temp = jsObject["temp"];
    this.weather = jsObject["weather"];
    this.way = jsObject["wayDistance"];
    this.wayTime = jsObject["wayTime"];
    this.stationNum = 4;
  }
}

class EventDate {
  int start;
  int end;
  DateTime startDate;
  DateTime endDate;
  DateFormat dateFormat = new DateFormat('dd.MM.yy');
  DateFormat timeFormat = new DateFormat('HH:mm');

  EventDate(start, end) {
    this.start = start;
    this.end = end;
    this.startDate = new DateTime.fromMillisecondsSinceEpoch(start*1000);
    this.endDate = new DateTime.fromMillisecondsSinceEpoch(end*1000);
  }

  String toDateString() {
    return dateFormat.format(startDate) +
        ' - ' +
        dateFormat.format(endDate);
  }

  String toTimeString() {
    return timeFormat.format(startDate) +
        ' - ' +
        timeFormat.format(endDate);
  }
}

class EventLocation {
  int zip;
  String city;
  double lat;
  double lon;

  EventLocation(zip, city, lat, lon) {
    this.zip = zip;
    this.city = city;
    this.lat = lat;
    this.lon = lon;
  }
}
